CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module is reated for helping keep your site clean of errors and notices.
Some drupal sites has one error that is repeat very frecuently so you can't see
any other errors or events on your watchdog.

This search php errors on every cron execution and show then into a single
page.

REQUIREMENTS
------------
This module requires the following modules:
 * dblog (core module)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module.
 * See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
This module currently has no configuration options.

MAINTAINERS
-----------
Current maintainers:
  * Alvaro J. Hurtado (alvar0hurtad0) - https://drupal.org/u/alvar0hurtad0
